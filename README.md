# EGG
Pre-Proposal: [LINK](https://docs.google.com/document/d/1IhxZGSYsbbrLO_yRFm3W4yaqQnbNJnppafjC5ba0SDM/edit?fbclid=IwAR0LyccsiEtnAbJaHREQAUSpRNBgPFrpYbyhwOpkQg79rqKL96wCjtWOh94#heading=h.qi0yq42r4srg)

Proposal: [LINK](https://docs.google.com/document/d/1Kb-On-NHru4WpyvOTXaB6ZTxBtgyNgKybWsLtOdyaKQ/edit?usp=sharing&fbclid=IwAR1vp3uYr2-uhnC4R8InZHaVxQEz_UeZsam9BTof5_4o56goIecyjoQE1B8)

Design: [LINK](https://docs.google.com/document/d/1_fEZASAyBrtcoW_L1BS0Em1Fd9o0QUpDzNEbIUdMylk/edit?usp=sharing)

Final Report: [LINK](https://docs.google.com/document/d/1TPP4IFPF4E89CaRcMiXyYctV1cM3LxbqvuZ4CDH_Hok/edit?usp=sharing)

Link to Poster: [LINK](https://drive.google.com/file/d/13d-gjqcu-u1q6cgFdwsxfpvC1J2yE-FQ/view?usp=sharing)

Link to Video Demo: [LINK](https://drive.google.com/file/d/10inQA6lP1BZ-dKk8BcDt6BMayzwv9TiP/view?usp=sharing)

## Tutorial ##
EGG is designed to allow users to quickly and easily visualize graphs in `Latex`. Egg also
enables users to run arbitrary algorithms over the graph and execute custom layout functions.

#### Creating a Graph ####
The first step in using EGG is to create a new `Graph` object. The `Graph` object is the
main structure in the language, and stores the graph data and layout. You can create a
`Graph` by running the follow code.

```javascript
let g = new Graph();
```

`Vertices` and `Edges` can be added to the graph like so:

```javascript
g.addVertices(['a', 'b', 'c']);
g.addEdges([['a', 'b'], ['b', 'c', 'label']]);
```
The `addVertices` function takes a list of strings representing each new node and its label.
The `addEdges` function takes a list of lists, where each sub-list represents the start node,
end node, and optional label of the edge.

To output `LaTex` code representing the graph, run:
```javascript
g.output()
```
This will return a string of executable `LaTeX` which plots the graph.

#### Formatting Nodes and Edges ####
It is also possible to provide custom formating for specific nodes and edges. The
`Vertex` and `Edge` objects of a `Graph` can be accessed functions such as
`getEdges()`, `getVertices`, `getNeighbors` etc.

Each `Vertex` and `Edge` object has special formatting functionality which enable users to easily
customize the coordinates and formatting. For example, users can change the color and size of
a vertex, or the color and width of an edge. 

To change all the nodes in a `Graph` to red, and increase the width of every `Edge` run:
```javascript
for (let v of g.getVertices()) {
    v.setFill('red');
}

for (let e of e.getEdges()) {
    e.setThickness(0.5);
}
```
More detail about formatting options can be found in the documentation.


#### Visualizing a Graph ####
The key functionality that Egg provides is the ability to easily visualize graphs in `LaTeX`. The
default layout visualization is an exploratory visualization. A standard visualization is plotted, 
and the user can easily manipulate nodes or edges to build the
exact graph they want. The user can  call the `debugPosition` function to see the exact
`(x, y)` coordinates of the nodes, and manipulate the accordingly. The exact `x` and `y` position
of a `Vertex` can be changed using the `setX` and `setY` functions.

Additionally, specialized graphs can be plotted using built-in layout functions. For example,
for bi-partite or acyclic graphs, the user can specify a bi-paritite or topological layout, and
Egg generates an intuitive visualization.

If the built-in layouts do not suffice, users can specify their own custom layout functions. This
can be done by passing a custom layout to the `setLayout` function. To write a custom layout, 
users must define a function that takes a `Graph` object and updates the `x` and `y` coordinate
of every node in the graph. More detail about layout functions can be found in the documentation.

#### Algorithms ####
Users also have the power to run common graph algorithms over their Egg `Graph`. This can
be used to plot specialized layouts or visualize the behavior of common algorithms. For example,
using the built in `bfs` function, a user can run a breadth-first-search over their `Graph`, and
plot each new level in a darker shade. Thus, when the graph is plotted, the behavior of  `bfs` is
intuitive and easy to grasp.

Egg exposes helper functions, such as `getNeighbors` and `getInDegree` which can be used when running algorithms over a `Graph`.

#### Composing Graphs ####
Another key feature of Egg is the ability to compose two disjoint `Graphs` by defining `Edges`
between them. This can be used to highlight sub-sections of a `Graph`, or quickly combine two
previouly created `Graph` structures.

To compose two `Graphs` together, run the following code. Assumne that we have already defined
`Graphs` `g1` and `g2`.
```javascript
g3 = g1.compose(g2, [['a', 'b']);
```

The compose function takes a `Graph`, and a list of `Edge` connections. It returns a new `Graph`
structure which represents the composition of the two `Graphs`, connected by the given `Edge`.

## Examples ##
### Example 1: Simple ###

```javascript
let g = new Graph()
    .addVertices(['a', 'b', 'c'])
    .addEdges([['a', 'b'], ['b', 'c']])
    .undirected()
    .debugPosition(); // We want to see the positions on an implicit grid

let vertA = g.getVertex('a'); // Not happy with the position of the 'a' node
vertA.setX(5);                // Change the x coordinate directly.

let tikz = g.output();
console.log(tikz);
```

### Example 2: Intermediate ###
#### Layout File: ####

```javascript
export default function(g : Graph) : void {
    let offset = 0;
    for (let v of g.getVertices()) {
        v.setX(0);
        v.setY(offset);
        offset += 3;
    }
}
```

#### Client code: ####
```javascript
let g = new Graph()
    .addVertices(['a', 'b', 'c', 'd', 'e', 'f'])
    .addEdges([['a', 'b'], ['b', 'c'], ['c','d'], ['d', 'e'], ['e', 'f']])
    .setLayout(stackedLayout); // Assumes the Layout the client made is imported as stackedLayout

let tikz = g.output();
console.log(tikz);
```

### Example 3: Intermediate ###

```javascript
let g = new Graph()
    .addVertices(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'i'])
    .addEdges([['g', 'i'], ['a', 'b'], ['a','c'], ['a', 'd'], ['b', 'e'], ['b', 'f'], ['f', 'g']])
    .undirected();

let bfs = g.bfs(g.getVertex('a')); // Use a utility function to get the bfa ordering
for (let i = 0; i < bfs.length; i++) { // Color nodes lighter that were visited later
    bfs[i].setFill('red');
    bfs[i].setOpacity(1 - (0.12 * i));
}

let tikz = g.output();
console.log(tikz);
```

### Example 4: More advanced ###

#### Layout File: ####

```javascript
import Graph from "../graph";
import Vertex from "../vertex";

export default function(g : Graph) : void {
    var a = [], b = [];
    bipartitePartition(g, a, b);
    let offset = 0;
    
    for (let v of a) {
        v.setX(0);
        v.setY(offset);
        offset += 2;
    }

    offset = 0;
    for (let v of b) {
        v.setX(6);
        v.setY(offset);
        offset += 2;
    }
}

function bipartitePartition(g: Graph, a: Array<Vertex>, b: Array<Vertex>) : void {
    var visited = new Set();
    let vertices = g.getVertices();
    var start = vertices[0];
    var cur = [];
    cur.push(start);
    let twoColor = new Map();
    twoColor.set(start, true);

    // Perform a two-coloring of the graph
    while (cur.length != 0) {
        let v = cur.shift()
        visited.add(v);
        for (var u of g.getNeighbors(v)) {
            if (!visited.has(u)) {
                twoColor.set(u, !twoColor.get(v));
                cur.push(u);
            }
        }
    }

    for (var v of vertices) {
        console.log(v.getLabel());
        console.log(twoColor.get(v));
        if (twoColor.get(v)) {
            a.push(v);
        } else {
            b.push(v);
        }
    }
}
```

#### Client code: ####

```javascript
let g = new Graph()
    .addVertices(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'])
    .addEdges([['a','g'], ['a', 'e'], ['b', 'e'], ['d', 'f'], ['d', 'g'], ['b', 'h'], ['c', 'h']])
    .undirected()
    .setLayout(bipartiteLayout);

let g2 = new Graph()
    .addVertices(['i', 'j', 'k', 'l', 'm', 'n'])
    .addEdges([['i', 'l'], ['i', 'm'], ['i', 'n'], ['j', 'l'], ['k', 'm']])
    .undirected()
    .setLayout(bipartiteLayout);

let g3 = g.compose(g2, [['a', 'm']])
    .undirected()
    .setLayout(bipartiteLayout);

let tikz = g3.output();
console.log(tikz);

```