import Template from "../src/layouteng";
import Graph from "../src/graph";

let g = new Graph();
g.addVertices(['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']);
g.isUndirected();
g.addEdges([['a', 'e'], ['a','g'], ['b', 'e'], ['d', 'f'], ['d', 'g'], ['b', 'h'], ['c', 'h']]);

let layout = new Template();
layout.setLayout("bipartite");
g.setTemplate(layout);

let tikz = g.output();
console.log(tikz);