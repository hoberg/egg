import Template from "../src/layouteng";
import Graph from "../src/graph";
import Edge from "../src/edge";
import Vertex from "../src/vertex";
import {expect} from "chai";

describe("Graph Composure Tests", () => {
    it("Compose larger graph with smaller graph", () => {
        let large: Graph = new Graph();
        large.addVertices(['A', 'B', 'C', 'D']);
        large.addEdges([['A', 'B'], ['C', 'D']]);
        
        let small: Graph = new Graph();
        small.addVertices(['E', 'F']);
        small.addEdges([['E', 'F']])
        let composed: Graph = large.compose(small, [['A', 'E']]);
        expect(composed.getAdjMatrix()).to.be.eql([[ 0, 1, 0, 0, 1, 0 ],
                                                   [ 0, 0, 0, 0, 0, 0 ],
                                                   [ 0, 0, 0, 1, 0, 0 ],
                                                   [ 0, 0, 0, 0, 0, 0 ],
                                                   [ 0, 0, 0, 0, 0, 1 ],
                                                   [ 0, 0, 0, 0, 0, 0 ] ]);
        let vertices: Vertex[] = []
        let labels: Map<string, Vertex> = new Map();
        vertices.push(new Vertex('A', 0));
        vertices.push(new Vertex('B', 1));
        vertices.push(new Vertex('C', 2));
        vertices.push(new Vertex('D', 3));
        vertices.push(new Vertex('E', 4));
        vertices.push(new Vertex('F', 5));
        labels.set('A', new Vertex('A', 0));
        labels.set('B', new Vertex('B', 1));
        labels.set('C', new Vertex('C', 2));
        labels.set('D', new Vertex('D', 3));
        labels.set('E', new Vertex('E', 4));
        labels.set('F', new Vertex('F', 5));

        expect(composed.getVertices()).to.be.eql(vertices);
        expect(composed.getLabels()).to.be.eql(labels);
    });

    it("Throws error for duplicate node labels", () => {
        function f() {
            let g1: Graph = new Graph();
            let g2: Graph = new Graph();

            g1.addVertices(['A']);
            g2.addVertices(['A']);

            g1.compose(g2, []);
            
            return null;
        }
        expect(f).to.throw(Error);
    });
});