import Graph from "../src/graph";
import topologicalLayout from "../src/layout/topological";

let g = new Graph()
    .addVertices(['a', 'b', 'c', 'd', 'e', 'f'])
    .addEdges([['a', 'b'], ['a','d'], ['b', 'c'], ['d', 'e'], ['e', 'f'], ['c', 'f']])
    .setLayout(topologicalLayout);

let tikz = g.output();
console.log(tikz);