import Template from "../src/layouteng";
import Graph from "../src/graph";
import {expect} from "chai";

const correctTikz: string = "\\documentclass[12pt]{article}\n\\usepackage{tikz}\n\n\\usepackage{tkz-euclide}\n\n\\usetkzobj{all}\n\n\\begin{document}\n\n\\begin{center}\n\\begin{tikzpicture}[scale=0.2]\n\\tikzstyle{every node}+=[inner sep=0pt]\n\\draw [black] (0,10) circle (3);\n\\draw (0,10) node {$A$};\n\\draw [black] (10,10) circle (3);\n\\draw (10,10) node {$B$};\n\\draw [black] (30,10) circle (3);\n\\draw (30,10) node {$C$};\n\\draw [black] (20,10) circle (3);\n\\draw (20,10) node {$D$};\n\\draw [black] (40,10) circle (3);\n\\draw (40,10) node {$E$};\n\\draw [black] (50,10) circle (3);\n\\draw (50,10) node {$F$};\n\\tkzDefPoint(3,10){A}\\tkzDefPoint(5,9.9){AB}\\tkzDefPoint(7,10){B}\n\\tkzCircumCenter(A,AB,B)\\tkzGetPoint{O}\n\\tkzDrawArc(O,A)(B)\n\n\\tkzDefPoint(10,7){B}\\tkzDefPoint(20,2){BC}\\tkzDefPoint(30,7){C}\n\\tkzCircumCenter(B,BC,C)\\tkzGetPoint{O}\n\\tkzDrawArc(O,B)(C)\n\n\\tkzDefPoint(0,7){A}\\tkzDefPoint(10,2){AD}\\tkzDefPoint(20,7){D}\n\\tkzCircumCenter(A,AD,D)\\tkzGetPoint{O}\n\\tkzDrawArc(O,A)(D)\n\n\\tkzDefPoint(20,7){D}\\tkzDefPoint(30,2){DE}\\tkzDefPoint(40,7){E}\n\\tkzCircumCenter(D,DE,E)\\tkzGetPoint{O}\n\\tkzDrawArc(O,D)(E)\n\n\\tkzDefPoint(43,10){E}\\tkzDefPoint(45,9.9){EF}\\tkzDefPoint(47,10){F}\n\\tkzCircumCenter(E,EF,F)\\tkzGetPoint{O}\n\\tkzDrawArc(O,E)(F)\n\n\\tkzDefPoint(30,7){C}\\tkzDefPoint(40,2){CF}\\tkzDefPoint(50,7){F}\n\\tkzCircumCenter(C,CF,F)\\tkzGetPoint{O}\n\\tkzDrawArc(O,C)(F)\n\n\\end{tikzpicture}\n\\end{center}\n\n\\end{document}"

describe("TopoSort Tests", () => {
    it("Produces correct topo ordering complex", () => {
        let g = new Graph();
        let vertices: string[] = ['A', 'B', 'C', 'D', 'E', 'F']
        g.addVertices(vertices);
        g.addEdges([['A', 'B'], ['B', 'C'], ['A', 'D'], ['D', 'E'], ['E', 'F'], ['C', 'F']]);
        let layout = new Template();
        layout.topoLayout();
        g.setTemplate(layout);
        g.output();

        expect(g.getVertex('A').getX()).to.be.equal(0);
        expect(g.getVertex('A').getY()).to.be.equal(10);

        expect(g.getVertex('B').getX()).to.be.equal(10);
        expect(g.getVertex('B').getY()).to.be.equal(10);

        expect(g.getVertex('C').getX()).to.be.equal(30);
        expect(g.getVertex('C').getY()).to.be.equal(10);

        expect(g.getVertex('D').getX()).to.be.equal(20);
        expect(g.getVertex('D').getY()).to.be.equal(10);

        expect(g.getVertex('E').getX()).to.be.equal(40);
        expect(g.getVertex('E').getY()).to.be.equal(10);

        expect(g.getVertex('F').getX()).to.be.equal(50);
        expect(g.getVertex('F').getY()).to.be.equal(10);

    });

    it("Produces correct tikz code", () => {
        let g = new Graph();
        g.addVertices(['A', 'B', 'C', 'D', 'E', 'F']);
        g.addEdges([['A', 'B'], ['B', 'C'], ['A', 'D'], ['D', 'E'], ['E', 'F'], ['C', 'F']]);
        let layout = new Template();
        layout.topoLayout();
        g.setTemplate(layout);
        let tikz = g.output();
        expect(tikz).to.be.equal(correctTikz);
    });

    it("Throws error for non-topological graph", () => {
        function f() {
            let g = new Graph();
            g.addVertices(['A', 'B', 'C']);
            g.addEdges([['A', 'B'], ['B', 'C'], ['C', 'A']]);
            let layout = new Template();
            layout.topoLayout();
            g.setTemplate(layout);
            return g.output();
        }
        expect(f).to.throw(Error);
    });
});