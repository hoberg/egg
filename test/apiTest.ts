import Template from "../src/layouteng";
import Graph from "../src/graph";
import Edge from "../src/edge";
import Vertex from "../src/vertex";
import {expect} from "chai";

describe("Graph Tests", () => {
    it("Empty graph", () => {
        let g: Graph = new Graph();
        expect(g.getEdges().length).to.be.equal(0);
        expect(g.getVertices().length).to.be.equal(0);
        expect(g.getAdjMatrix().length).to.be.equal(0);
        expect(g.getLabels().size).to.be.equal(0);
        expect(g.getTemplate() == null).to.be.true;;
    });

    it ("Standard graph", () => {
        let g: Graph = new Graph();
        g.addVertices(['A', 'B', 'C', 'D', 'E', 'F']);
        g.addEdges([['A', 'B', 'big edge'], ['A', 'F'], ['D', 'D', 'SELF']]);

        let vertices: Vertex[] = []
        let labels: Map<string, Vertex> = new Map();
        let edges: Edge[] = [];
        let a: Vertex = new Vertex('A', 0);
        let b: Vertex = new Vertex('B', 1);
        let c: Vertex = new Vertex('C', 2);
        let d: Vertex = new Vertex('D', 3);
        let e: Vertex = new Vertex('E', 4);
        let f: Vertex = new Vertex('F', 5);

        vertices.push(a);
        vertices.push(b);
        vertices.push(c);
        vertices.push(d);
        vertices.push(e);
        vertices.push(f);
        edges.push(new Edge(a, b, "big edge"));
        edges.push(new Edge(a, f, null));
        edges.push(new Edge(d, d, "SELF"));
        labels.set('A', a);
        labels.set('B', b);
        labels.set('C', c);
        labels.set('D', d);
        labels.set('E', e);
        labels.set('F', f);

        expect(g.getVertices()).to.be.eql(vertices);
        expect(g.getLabels()).to.be.eql(labels);
        expect(g.getEdges()).to.be.eql(edges);
        expect(g.getVertex('A')).to.be.eql(a);
        expect(g.getAdjMatrix()).to.be.eql([[0, 1, 0, 0, 0, 1],
                                            [0, 0, 0, 0, 0, 0],
                                            [0, 0, 0, 0, 0, 0],
                                            [0, 0, 0, 1, 0, 0],
                                            [0, 0, 0, 0, 0, 0],
                                            [0, 0, 0, 0, 0, 0]]);
    });

    it ("Updated graph", () => {
        let g: Graph = new Graph();
        g.addVertices(['A', 'B', 'C', 'D', 'E', 'F']);
        g.addEdges([['A', 'B', 'big edge'], ['A', 'F'], ['D', 'D', 'SELF']]);
        g.addVertices(['H', 'I']);
        g.addEdges([['H', 'H', 'new edge'], ['A', 'I']]);

        let vertices: Vertex[] = []
        let labels: Map<string, Vertex> = new Map();
        let edges: Edge[] = [];
        let a: Vertex = new Vertex('A', 0);
        let b: Vertex = new Vertex('B', 1);
        let c: Vertex = new Vertex('C', 2);
        let d: Vertex = new Vertex('D', 3);
        let e: Vertex = new Vertex('E', 4);
        let f: Vertex = new Vertex('F', 5);
        let h: Vertex = new Vertex('H', 6);
        let i: Vertex = new Vertex('I', 7);
        vertices.push(a);
        vertices.push(b);
        vertices.push(c);
        vertices.push(d);
        vertices.push(e);
        vertices.push(f);
        vertices.push(h);
        vertices.push(i);
        edges.push(new Edge(a, b, "big edge"));
        edges.push(new Edge(a, f, null));
        edges.push(new Edge(d, d, "SELF"));
        edges.push(new Edge(h, h, "new edge"));
        edges.push(new Edge(a, i, null));
        labels.set('A', a);
        labels.set('B', b);
        labels.set('C', c);
        labels.set('D', d);
        labels.set('E', e);
        labels.set('F', f);
        labels.set('H', h);
        labels.set('I', i);

        expect(g.getVertices()).to.be.eql(vertices);
        expect(g.getLabels()).to.be.eql(labels);
        expect(g.getEdges()).to.be.eql(edges);
        expect(g.getVertex('A')).to.be.eql(a);
        expect(g.getAdjMatrix()).to.be.eql([[0, 1, 0, 0, 0, 1, 0, 1],
                                            [0, 0, 0, 0, 0, 0, 0, 0],
                                            [0, 0, 0, 0, 0, 0, 0, 0],
                                            [0, 0, 0, 1, 0, 0, 0, 0],
                                            [0, 0, 0, 0, 0, 0, 0, 0],
                                            [0, 0, 0, 0, 0, 0, 0, 0],
                                            [0, 0, 0, 0, 0, 0, 1, 0],
                                            [0, 0, 0, 0, 0, 0, 0, 0]]);
    });
});