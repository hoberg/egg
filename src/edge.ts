import Vertex from "./vertex";

export default class Edge {
    private start: Vertex;
    private end: Vertex;
    private weight: number;
    private thickness: number;
    private color: string;
    private edge: Array<String>;

    /**
     * Constructs a new edge from start to end with the given weight
     * @param start the start of this edge
     * @param end the end of this edge
     * @param weight the weight of this edge
     */
    constructor(start: Vertex, end: Vertex, weight: number) {
        this.start = start;
        this.end = end;
        this.weight = weight;
        this.edge = [];
        this.thickness = null;
        this.color = '';
    }

    /**
     * @returns a copy of this Edge
     */
    public clone(): Edge {
        let res: Edge = new Edge(this.start.clone(), this.end.clone(), this.weight);
        res.weight = this.weight;
        res.color = this.color;
        res.edge = this.edge;
        return res;
    }

    /**
     * @returns the Vertex at the start of this edge
     */
    public getStart(): Vertex {
        return this.start;
    }

    /**
     * @returns the Vertex at the end of this edge
     */
    public getEnd(): Vertex {
        return this.end;
    }

    /**
     * @returns the weight of this edge
     */
    public getWeight(): number {
        return this.weight;
    }

    /**
     * Makes this edge bend to the left
     */
    public bendLeft(): void {
        this.edge.push('bend left');
    }

    /**
     * Makes this edge bend to the right
     */
    public bendRight(): void {
        this.edge.push('bend right');
    }

    /**
     * Makes this edge directed
     */
    public directed(): void {
        this.edge.push('->');
    }

    /**
     * Generates and returns a LaTeX string based on the properties of this Edge that can
     * be added to a LateX figure to visualize this Edge.
     * @returns the LaTeX string representing this Edge.
     */
    public getEdgeString(): string {
        if (this.edge.length === 0) {
            return '';
        }
        let res: string = '[' + this.edge[0];
        for (let i = 1; i < this.edge.length; i++) {
            res += ', ' + this.edge[i];
        }

        if (this.thickness != null) {
            res+= ', line width=' + this.thickness + 'mm';
        }

        if (this.color !== '') {
            res += ', ' + this.color;
        }

        res += ']';
        return res;
    }

    /**
     * Sets the thickness of this edge to the given number (in mm)
     * @param n the thickness, in mm, to make this edge
     */
    public setThickness(n: number): void {
        this.thickness = n;
    }

    /**
     * Sets the color of this edge to the given color
     * @param color the color to make this edge
     */
    public setColor(c: string): void {
        this.color = c;
    }
}