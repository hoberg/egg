export default class Template {
    private vProps: Function[];
    private eProps: Function[];
    private layout: string;

    constructor() {
        this.vProps = [];
        this.eProps = [];
        return this;
    }

    public applyVertices(fn: Function): Template {
        this.vProps.push(fn);
        return this;
    }

    public applyEdges(fn: Function): Template {
        this.eProps.push(fn);
        return this;
    }

    public setLayout(layout: string): void {
        this.layout = layout;
    }

    public topoLayout(): void {
        this.layout = 'topo';
    }

    public getLayout() : string {
        return this.layout;
    }
}