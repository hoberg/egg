import Graph from "../graph";
import Vertex from "../vertex";

export default function(g: Graph) : void {
    let sorted: Vertex[] = g.topoSort();

    if (sorted === null) {
        throw Error("Graph cannot be sorted topologically");
    }

    let offset: number = 0;
    let index: number = 0;
    let topoIndex = new Map();
    for (let v of sorted) {
        v.setX(offset);
        v.setY(0);
        topoIndex.set(v, index);
        index += 1;
        offset += 3;
    }

    let count: number = 0;
    for (let e of g.getEdges()) {
        if (topoIndex.get(e.getEnd()) - topoIndex.get(e.getStart()) !== 1) {
            if (count % 2 == 0) {
                e.bendLeft();
            } else {
                e.bendRight();
            }
            count += 1;
        }
    }
}