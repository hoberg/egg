import Graph from "../graph";
import Vertex from "../vertex";

export default function(g : Graph) : void {
    var a = [], b = [];
    bipartitePartition(g, a, b);
    let offset = 0;
    
    for (let v of a) {
        v.setX(0);
        v.setY(offset);
        offset += 2;
    }

    offset = 0;
    for (let v of b) {
        v.setX(6);
        v.setY(offset);
        offset += 2;
    }
}

function bipartitePartition(g: Graph, a: Array<Vertex>, b: Array<Vertex>) : void {
    var visited = new Set();
    let vertices = g.getVertices();
    var start = vertices[0];
    var cur = [];
    cur.push(start);
    let twoColor = new Map();
    twoColor.set(start, true);

    // Perform a two-coloring of the graph
    while (cur.length != 0) {
        let v = cur.shift()
        visited.add(v);
        for (var u of g.getNeighbors(v)) {
            if (!visited.has(u)) {
                twoColor.set(u, !twoColor.get(v));
                cur.push(u);
            }
        }
    }

    for (var v of vertices) {
        if (twoColor.get(v)) {
            a.push(v);
        } else {
            b.push(v);
        }
    }
}