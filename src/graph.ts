import Vertex from "./vertex";
import Edge from "./edge";

var seed = 3;
function random() {
    var x = Math.sin(seed++) * 100000;
    return x - Math.floor(x);
}

export default class Graph {
    private vertices: Vertex[];
    private edges: Edge[] = [];
    private graph: number[][];
    private labels: Map<string, Vertex>;
    private directed: boolean;
    private layout: any;
    private debug: boolean;

    /**
     * Constructs an empty directed graph with a default layout scheme.
     */
    constructor() {
        this.vertices = [];
        this.edges = [];
        this.graph = [];
        this.labels = new Map();
        this.directed = true;
        this.layout = this.defaultLayout;
        this.debug = false;
    }

    /**
     * Makes all edges in this graph undirected, and makes any edges added in the future undirected.
     * @returns a reference to this graph
     */
    public undirected() : Graph {
        this.directed = false;
        
        // If edges were already added, we must make them undirected edges.
        for (let e of this.edges) {
            let startVertex = e.getStart();
            let endVertex = e.getEnd();
            this.graph[endVertex.getIndex()][startVertex.getIndex()] = 1;
        }
        return this;
    }

    /**
     * Adds vertices to this graph represented by a given list of labels
     * @param labels the list of vertices to add to this graph
     * @returns a reference to this graph
     */
    public addVertices(labels: string[]) : Graph {
        let len: number = labels.length;
        let numVertices = this.vertices.length;
        if (this.graph.length != 0) {
            len = this.graph.length + labels.length;
            for (let i: number = 0; i < this.graph.length; i++) {
                let row: number[] = this.graph[i];
                for (let j: number = 0; j < labels.length; j++) {
                    row.push(0);
                }
            }
        }

        for (let i: number = 0; i < labels.length; i++) {
            this.graph.push(Array(len).fill(0));
            let v = new Vertex(labels[i], i + numVertices);
            this.labels.set(labels[i], v);
            this.vertices.push(v);
        }

        return this;
    }

    /**
     * Adds edges to this graph represented by a given list of lists. The given
     * list contains items that are lists with the following format [v1, v2, w]
     * where v1 corresponds to the start vertex of an edge, v2 corresponds to the
     * end vertex of an edge, and w corresponds to the weight of an edge.
     * @param edges The list of lists representing the edges
     * @returns a reference to this graph
     */
    public addEdges(edges: any[][]) : Graph {
        for (let edge of edges) {
            let startLabel: string = edge[0];
            let endLabel: string = edge[1];
            let weight: number = null;
            if (edge.length > 2) {
                weight = edge[2];
            }
            if (!this.labels.has(startLabel) || !this.labels.has(endLabel)) {
                throw new Error('Cannot add edge between nodes (' + startLabel
                                + ', ' + endLabel + ') that do not exist');
            }
            let startVertex: Vertex = this.labels.get(startLabel);
            let endVertex: Vertex = this.labels.get(endLabel);

            let e: Edge = new Edge(startVertex, endVertex, weight);
            this.edges.push(e);

            this.graph[startVertex.getIndex()][endVertex.getIndex()] = 1;
            if (!this.directed) {
                this.graph[endVertex.getIndex()][startVertex.getIndex()] = 1;
            }
        }
        return this;
    }

    /**
     * Replaces the current layout with a specified layout. The layout must be a function 
     * that accepts a Graph as a parameter.
     * When called, the layout function is responsible for setting any relevant properties
     * of the vertices/edges of the given graph.
     * @param layout the layout to use
     * @returns a reference to this graph
     */
    public setLayout(layout: any): Graph {
        this.layout = layout;
        return this;
    }

    /**
     * Enables position debugging. When position debugging is enabled, this graph will contain a
     * grid and vertices will be labeled with their coordinates on the grid.
     * @returns a reference to this graph
     */
    public debugPosition() : Graph {
        this.debug = true;
        return this;
    }

    /**
     * Generates the LaTeX visualization of this graph. Relies on properties set by the layout
     * function to determine how to visualize nodes.
     * @returns A string that, when printed, represents the exact LaTeX code to visualize this
     * graph.
     */
    public output() : string {
        this.layout(this); // Lays out nodes and edges as specified (default if not specified)

        let latex: string = "\\documentclass[12pt]{article}\n"
        + "\\usepackage{tikz}\n"
        + "\\begin{document}\n"
        + "\\begin{tikzpicture}\n"
        + "\\tikzstyle{vertex}=[draw, circle]\n";

        if (this.debug) { // client wants to debug information
            latex += this.generateDebugGraph();
        }
        
        for (let v of this.vertices) {
            let label = v.getLabel();
            latex += "\\node" + v.getLayout() + " (" + label + ") at (" + v.getX() + "," + v.getY() + ") [vertex, label=" + label + "] {};\n"
        }
        
        for (let e of this.edges) {
            let start = e.getStart().getLabel();
            let end = e.getEnd().getLabel();
            let label = e.getWeight() == null ? 'to ' : 'edge node [yshift=4mm] {\\tt ' + e.getWeight() + '} ';
            if (this.directed) {
                e.directed();
            }
            let edgeStr = e.getEdgeString();

            latex += "\\draw " + edgeStr + " (" + start + ") " + label + "(" + end + ");\n"
        }

        latex += "\\end{tikzpicture}\n"
            + "\\end{document}";
        return latex;
    }

    /**
     * Replaces current vertex labels with new labels to assist with debugging position and
     * generates a LaTeX grid.
     * @returns A LaTeX string that can be added to a LaTeX figure that will render a grid of
     * positions in the output.
     */
    private generateDebugGraph() : string {
        let minX = Number.MAX_SAFE_INTEGER, minY = Number.MAX_SAFE_INTEGER;
        let maxX = Number.MIN_SAFE_INTEGER, maxY = Number.MIN_SAFE_INTEGER;
        let offset = 3
        for (let v of this.vertices) {
            minX = Math.min(minX, v.getX());
            minY = Math.min(minY, v.getY());
            maxX = Math.max(maxX, v.getX());
            maxY = Math.max(maxY, v.getY());
            v.setLabel(v.getLabel() + "{(" + v.getX() + "," + v.getY() + ")}");
        }
        minX = Math.min(offset, minX);
        minY = Math.min(offset, minY);
        return "\\draw[step=1.0,gray,thin] (" + (minX - offset) + "," + (minY - offset) + ") grid (" + (maxX + offset) + "," + (maxY + offset) + ");\n"
            + "\\draw[line width=0.4mm, blue] (" + (minX - offset) + ", 0) -- (" + (maxX + offset) + ", 0);\n"
            + "\\draw[line width=0.4mm, blue] (0, " + (minY - offset) + ") -- (0, " + (maxY + offset) + ");\n";
    }

    /**
     * The default layout function for a generic graph. Randomly positions vertices and connects
     * them with straight lines.
     */
    private defaultLayout(): void  {
        let h: number = this.vertices.length;
        for (let v of this.vertices) {
            let x: number = this.getRandomInt(12);
            let y: number = this.getRandomInt(h);
            if (v.getX() == null) { // Don't override client-specified points.
                v.setX(x);
            }
            if (v.getY() == null) { // Don't override client-specified points.
                v.setY(y);
            }
        }
    }

    /**
     * Returns a random integer between 0 inclusive and max exclusive
     * @param max the maximum (exclusive) number that this function can generate.
     */
    private getRandomInt(max) {
        return Math.floor(random() * Math.floor(max));
    }

    /**
     * Generates and returns a topological sort of this graph, if one exists, null otherwise.
     * @returns a Vertex[] representing the topological sort, where the Vertex at index 0 is
     * the first Vertex in the topological sort, or null if no topological sort exists.
     */
    public topoSort(): Vertex[] {
        let s: Vertex[] = [];
        let visited: boolean[] = Array(this.vertices.length).fill(false);
        let in_degree: number[] = [];

        for (let i = 0; i < this.vertices.length; i++) {
            in_degree.push(this.getInDegree(this.vertices[i]));
        }

        let q: Vertex[] = [];

        for (let i = 0; i < this.vertices.length; i++) {
            if (in_degree[i] == 0) {
                q.push(this.vertices[i]);
                visited[i] = true;
            }
        }

        while (q.length != 0) {
            let vertex: Vertex = q.shift();
            s.push(vertex);
            for (let neighbor of this.getNeighbors(vertex)) {
                let index: number = this.vertices.indexOf(neighbor);
                if (!visited[index]) {
                    in_degree[index] -= 1;
                    if (in_degree[index] == 0) {
                        q.push(neighbor);
                        visited[index] == true;
                    }
                }
            }
        }
        
        if (s.length != this.vertices.length) {
            return null;
        }
        return s;
    }

    /**
     * Performs a breadth-first search of this graph starting at vertex source
     * @param source the source of the BFS
     */
    public bfs(source: Vertex): Vertex[] {
        let visited = new Set();
        let out = [];
        let cur = [];
        cur.push(source);
        while (cur.length > 0) {
            let v = cur.shift();
            visited.add(v);
            out.push(v);
            for (let u of this.getNeighbors(v)) {
                if (!visited.has(u)) {
                    cur.push(u);
                }
            }
        }
        return out;
    }

    /**
     * Performs a depth-first search of this graph starting at vertex source
     * @param source the source of the DFS
     */
    public dfs(source: Vertex): Vertex[] {
        let visited = new Set();
        let out = [];
        let cur = [];
        cur.push(source);
        while (cur.length > 0) {
            let v = cur.pop();
            visited.add(v);
            out.push(v);
            for (let u of this.getNeighbors(v)) {
                if (!visited.has(u)) {
                    cur.push(u);
                }
            }
        }
        return out;
    }

    /**
     * Returns the in degree of the given vertex
     * @param v the vertex to get the in degree of
     * @returns the in degree of v
     */
    public getInDegree(v: Vertex): number {
        let degree: number = 0;
        for (let i = 0; i < this.graph.length; i++) {
            degree += this.graph[i][v.getIndex()];
        }
        return degree;
    }

    /**
     * Returns the neighbors of the given vertex
     * @param v the vertex to get the neighbors of
     * @returns a Vertex[] containing all neighbors of v
     */
    public getNeighbors(v: Vertex): Vertex[] {
        let row: number[] = this.graph[v.getIndex()];
        let neighbors: Vertex[] = []
        for (let i = 0; i < row.length; i++) {
            if (row[i] != 0) {
                neighbors.push(this.vertices[i]);
            }
        }
        return neighbors;
    }

    /**
     * @returns a Vertex[] containing references to all of the vertices in this graph
     */
    public getVertices(): Vertex[] {
        return this.vertices;
    }

    /**
     * @param label the label of the vertex to find
     * @returns the Vertex associated with that label
     * @throws Error if no node exists with the given label
     */
    public getVertex(label: string): Vertex {
        if (!this.labels.has(label)) {
            throw new Error("No node in graph with given label");
        }
        return this.labels.get(label);
    }

    /**
     * @returns an Edge[] containing references to all of the edges in this graph
     */
    public getEdges(): Edge[] {
        return this.edges;
    }

    /**
     * Returns a new graph which is the result of composing this graph and g using
     * the given edges
     * @param g the graph to compose with this graph
     * @param edges a list of edges to compose on (ie. one vertex on the edge in this graph,
     * the other vertex on the edge in g)
     * @return a new Graph representing the result of the composition
     */
    public compose(g: Graph, edges: string[][]): Graph {
        this.verifyLabels(this.labels, g.labels);
        let result: Graph = new Graph();
        let totalVertices: number = this.vertices.length + g.vertices.length;
        result.graph = this.generateMatrix(totalVertices);
    
        // Populate new graph with edges from this graph
        for (let i = 0; i < this.graph.length; i++) {
            for (let j = 0; j < this.graph[i].length; j++) {
                if (this.graph[i][j] === 1) {
                    result.graph[i][j] = 1;
                }
            }
        }

        // Populate new graph with edges from given graph
        for (let i = 0; i < g.graph.length; i++) {
            for (let j = 0; j < g.graph[i].length; j++) {
                if (g.graph[i][j] === 1) {
                    result.graph[i + this.vertices.length][j + this.vertices.length] = 1;
                    
                }
            }
        }
        let appendEdges: any = function (e: Edge) {
            result.edges.push(e.clone());
        }

        this.vertices.forEach(function (v: Vertex) {
            result.vertices.push(v.clone());
        });

        for (let i = 0; i < g.vertices.length; i++) {
            let v: Vertex = g.vertices[i].clone();
            v.setIndex(i + this.vertices.length);
            result.vertices.push(v);
        }
        this.edges.forEach(appendEdges);
        g.edges.forEach(appendEdges);

        result.labels = this.cloneLabels(result.vertices);
        result.layout = this.layout;
        result.addEdges(edges);
        return result;
    }
    
    /**
     * Creates a mapping of vertex label -> vertex
     * @param vertices to use in the mapping
     * @returns a new map of vertex label -> vertex
     */
    private cloneLabels(vertices: Vertex[]): Map<string, Vertex> {
        let result: Map<string, Vertex> = new Map();
        vertices.forEach((v: Vertex) => {
            result.set(v.getLabel(), v);
        });
        return result;
    }

    /**
     * Verifies that no keys that appear in m1 appear in m2
     * @param m1 a map to verify
     * @param m2 a map to verify
     * @throws Error if m1 and m2 have the same key
     */
    private verifyLabels(m1: Map<string, Vertex>, m2: Map<string, Vertex>): void {
        m1.forEach((v: Vertex, label: string) => {
            if (m2.has(label)) {
                throw new Error('Cannot compose graphs with duplicate labels');
            }
        });

        m2.forEach((v: Vertex, label: string) => {
            if (m1.has(label)) {
                throw new Error('Cannot compose graphs with duplicate labels');
            }
        });
    }

    /**
     * Generates an n x n matrix of all zero entries.
     * @param n the width and height of the resulting matrix
     * @returns a number[][] of all zero entries of height and width n
     */
    private generateMatrix(n: number): number[][] {
        let matrix: number[][] = [];
        for (let i = 0; i < n; i++) {
            let row: number[] = [];
            for (let j = 0; j < n; j++) {
                row.push(0);
            }
            matrix.push(row);
        }
        return matrix;
    }
}