export default class Vertex {
    private label: string;
    private x: number;
    private y: number;
    private index: number;
    private fill: string;
    private size: number;
    private opacity: number;

    /**
     * Constructs a Vertex with the given label and index (index can be used in an
     * adjacency matrix implementation of a graph)
     * @param label the label to give this Vertex
     * @param index the index to give this Vertex
     */
    constructor(label: string, index: number) {
        this.label = label;
        this.x = null;
        this.y = null;
        this.index = index;
        this.fill = '';
        this.size = null;
        this.opacity = 1;
    }

    /**
     * @returns a copy of this Vertex
     */
    public clone(): Vertex {
        let res: Vertex = new Vertex(this.label, this.index);
        res.x = this.x;
        res.y = this.y;
        res.fill = this.fill;
        res.size = this.size;
        return res;
    }

    /**
     * @returns the label of this Vertex
     */
    public getLabel(): string {
        return this.label;
    }

    /**
     * Sets the label of this vertex to the given label
     * @param label the label to set for this Vertex
     */
    public setLabel(label: string): void {
        this.label = label;
    }

    /**
     * @returns the x coordinate of this Vertex
     */
    public getX(): number {
        return this.x;
    }

     /**
     * Sets the x coordinate of this vertex to the given x
     * @param x the x to set for this Vertex
     */
    public setX(x: number): void {
        this.x = x;
    }

    /**
     * @returns the y coordinate of this Vertex
     */
    public getY(): number {
        return this.y;
    }

    /**
     * Sets the y coordinate of this vertex to the given y
     * @param y the y to set for this Vertex
     */
    public setY(y: number): void {
        this.y = y;
    }

    /**
     * @returns the index of this Vertex
     */
    public getIndex(): number {
        return this.index;
    }

    /**
     * Sets the index of this vertex to the given index
     * @param index the index to set for this Vertex
     */
    public setIndex(index: number) {
        this.index = index;
    }

    /**
     * Sets the filll of this vertex to the given fill
     * @param fill the fill to set for this Vertex
     */
    public setFill(c: string): void {
        this.fill = c;
    }

    /**
     * Sets the size of this vertex to the given size (in cm)
     * @param size the size (in cm) to set for this Vertex
     */
    public setSize(n: number): void {
        this.size = n;
    }

    /**
     * Sets the opacity of this vertex to the given opacity
     * @param opacity the opacity to set for this Vertex
     */
    public setOpacity(n: number): void {
        this.opacity = Math.max(n, 0);
    }

    /**
     * Generates and returns a LaTeX string based on the properties of this Vertex that can
     * be added to a LateX figure to visualize this vertex.
     * @returns the LaTeX string representing this vertex.
     */
    public getLayout(): string {
        let res: string = '[';
        if (this.size != null) {
            res += 'minimum size=' + this.size + 'cm';
            if (this.fill !== '') {
                res += ', ';
            }
        }

        if (this.fill !== '') {
            res += 'fill=' + this.fill;
        }

        if (this.opacity != 1) {
            res += ', opacity=' + this.opacity;
        }

        res += ']'
        return res;
    }
}